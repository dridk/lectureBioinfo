# Conseils de lecture en lien avec la bioinfo

## Livres

- **The language of life** (Francis Collins): la découverte du gène BRCA1 dans la susceptibilité au cancer du sein, et l'impact des tests qui en découlent. Francis Collins a ensuite dirigé le Human Genome Project sur le premier séquençage du génome humain.
- **Le jeu de la science et du hasard -- la statistique et le vivant** (Daniel Schwartz) une introduction très claire aux principales notions de statistique.
- **La barque de Delphes -- ce que révèle le texte des génomes** (Antoine Danchin) Je ne suis pas allé jusqu'au bout mais j'en garde plutôt un bon souvenir et du coup j'ai envie de m'y remettre.
- **Il était une fois le gène** (Siddhartha Mukherjee) L'histoire de la génétique ; prix Pulitzer.
- **Bioinformatics algorithms -- an active learning approach** (Phillip Compeau and Pavel Pevzner) complément au cours cousera ; vidéos très chouettes.
- **Linked** (Albert-Laszlo Barabasi) sur les réseaux scale-free.
- **The Elements of Statistical Learning: Data Mining, Inference, and Prediction.**: [https://web.stanford.edu/~hastie/ElemStatLearn/](https://web.stanford.edu/~hastie/ElemStatLearn/) (Trevor Hastie, Robert Tibshirani and Jerome Friedman) très complet, disponible en PDF
- **Graph theory**: [http://diestel-graph-theory.com/](http://diestel-graph-theory.com/) (Reinhard Diestel) aussi disponible en PDF. **Difficile d'accès**
- **Théorie des graphes** (J.A Bondy et USR Murty) une traduction existe en Français : [http://www-sop.inria.fr/members/Frederic.Havet/Traduction-Bondy-Murty.pdf](http://www-sop.inria.fr/members/Frederic.Havet/Traduction-Bondy-Murty.pdf)
- **Machine learning avec scikit-learn** (Aurélien Géron)
- **Understanding bioinformatics** (Marketa Zvelebil and Jeremy O. Baum) explications très détaillées + mindmaps au début de chaque chapitre.
- **Speech and language processing** [https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf](https://web.stanford.edu/~jurafsky/slp3/ed3book.pdf) (D. Jurafsky et J.H. Martin)

## Ressources web
- **À la recherche des régions codantes**: [https://interstices.info/a-la-recherche-de-regions-codantes/](https://interstices.info/a-la-recherche-de-regions-codantes/)

